# Polyglot Translations

This project contains all translations needed for web and mobile, including app store texts.

## Translating

The translations are done using the [Crowdin project: Polyglot](https://crowdin.com/project/polyglot).

Crowdin automatically creates merge requests that can be merged into this project.

## Publishing translations

Publishing is done using [polyglot-uploader](https://gitlab.com/human.solutions/polyglot/crates/polyglot-uploader)
which uploads the translations to a folder in the Polyglot CDN.
